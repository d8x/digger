from setuptools import setup

setup(
    name='digger',
    version='0.1',
    packages=['digger'],
    url='',
    license='Apache License 2.0',
    author='d8x',
    author_email='',
    description='Testing framework'
)
