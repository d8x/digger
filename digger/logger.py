import sys
import time
import os


class Bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class FrameworkLogger:
    def __init__(self, output=sys.stdout, module_name: str = None, verbose: bool = False):
        self.output = output
        self.verbose = verbose
        self.module_name = module_name
        self._date_prefix = time.asctime()
        if module_name is not None:
            self._main_prefix = "[DIGGER]" + "[{}]".format(self._date_prefix) + " [{}]".format(self.module_name)
        else:
            self._main_prefix = "[DIGGER]" + "[{}]".format(self._date_prefix)
        self._info_prefix = self._main_prefix + "[INFO] "
        self._error_prefix = self._main_prefix + "[ERROR] "
        self._warning_prefix = self._main_prefix + "[WARNING] "
        self._debug_prefix = self._main_prefix + "[DEBUG] "

    def set_module_name(self, module_name: str):
        self.module_name = module_name

    def print(self, value: str):
        self._log(Bcolors.OKGREEN, self._main_prefix, value)

    def info(self, value: str):
        self._log(Bcolors.OKGREEN, self._main_prefix, value)
    
    def error(self, value: str):
        print(Bcolors.FAIL + self._error_prefix + value, Bcolors.ENDC, file=self.output)

    def warning(self, value: str):
        self._log(Bcolors.WARNING, self._warning_prefix, value)
    
    def debug(self, value: str):
        self._log(Bcolors.OKBLUE, self._debug_prefix, value)

    def _log(self, colour: str, prefix: str, value: str):
        if not self.verbose:
            with open(os.devnull, 'w') as output:
                print(colour + prefix + value + Bcolors.ENDC, file=output)
        else:
            print(colour + prefix + value + Bcolors.ENDC, file=self.output)

