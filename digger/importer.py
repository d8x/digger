import importlib.machinery
from pre_action_set import PreActionSet
from post_action_set import PostActionSet
from logger import FrameworkLogger
from testset import TestSet
from typing import List
import os


class TestImporter:
    def __init__(self, preActions: PreActionSet, postActions: PostActionSet, tests: TestSet, flog: FrameworkLogger):
        self.preModules = preActions
        self.postModules = postActions
        self.tests = tests
        self.flog = flog
        self.flog.set_module_name(__class__.__name__)

    def import_all(self):
        self.__import_pre_actions()
        self.__import_tests()
        self.__import_post_actions()

    def __import_tests(self):
        for v in self.tests.modules:
            for f in v.files:
                self.flog.debug("NAME: " + v.name)
                filename = os.path.basename(f)
                self.flog.debug("Filename:  " + filename)
                self.flog.debug("FILE: " + f)
                module = importlib.machinery.SourceFileLoader(filename, f).load_module()
                v.executables.append(module)

    def __import_pre_actions(self):
        for v in self.preModules.modules:
            for f in v.files:
                self.flog.debug("PRE_PATH: " + f)
                filename = os.path.basename(f)
                module = importlib.machinery.SourceFileLoader(filename, f).load_module()
                v.executables.append(module)

    def __import_post_actions(self):
        for v in self.postModules.modules:
            for f in v.files:
                self.flog.debug("POST_PATH: " + f)
                filename = os.path.basename(f)
                module = importlib.machinery.SourceFileLoader(filename, f).load_module()
                v.executables.append(module)

    def get_tests_files_paths(self) -> List:
        names = []
        for n in self.tests.modules:
            names.append(n.paths)
        return names

    def get_pre_actions_files_paths(self) -> List:
        names = []
        for n in self.preModules.modules:
            names.append(n.paths)
        return names

    def get_post_actions_files_paths(self) -> List:
        names = []
        for n in self.postModules.modules:
            names.append(n.paths)
        return names


