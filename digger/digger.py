import argparse
from fs_walker import TestFSWalker
from logger import FrameworkLogger
from executor import TestExecutor
from formatter import RawFormatter, ASCIFormatter, HTMLFormatter

REPORT_FORMATS = {"RAW": RawFormatter,
                  "ASCI": ASCIFormatter,
                  "HTML": HTMLFormatter}


def parse_args():
    parser = argparse.ArgumentParser(description='Digger is a tool for executing integration tests')
    parser.add_argument('-d', '--directory', action='store', dest='directory', help='directory of tests', required=True)
    parser.add_argument('-v', '--verbose', help='verbose', action="store_true")
    parser.add_argument('-o', '--output', help='output format: raw, asci, html', action='store', dest='output',
                        default='RAW')
    args = parser.parse_args()
    return args


def match_formatter(args):
    for k, f in REPORT_FORMATS.items():
        if args.output.upper() == k:
            return f
    return None


if __name__ == "__main__":
    args = parse_args()
    logger = FrameworkLogger(module_name=__name__)
    if args.verbose:
        logger = FrameworkLogger(module_name=__name__, verbose=True)
    formatter = match_formatter(args)
    if formatter is None:
        print("Wrong type of formatter, please set proper one")
        exit(1)
    test_walker = TestFSWalker(args.directory, logger)
    tests_walker = test_walker.read_modules()
    tests_walker.import_all()
    test_executor = TestExecutor(logger, formatter)
    test_executor.run(tests_walker)
