
POST_ACTION_SUFFIX = "post_"


class PostActionSet:
    def __init__(self):
        self.modules = []

    def __str__(self):
        return self.modules
    
    def __len__(self):
        return len(self.modules)