from typing import Dict
import os
from post_action_set import PostActionSet, POST_ACTION_SUFFIX
from pre_action_set import PreActionSet, PRE_ACTION_SUFFIX
from testset import TestSet, TESTS_SUFFIX
from importer import TestImporter
from module import Module
from logger import FrameworkLogger


class TestFSWalker:
    def __init__(self, tests_location: str, flog: FrameworkLogger):
        self.__tests_location = os.path.abspath(tests_location)
        self.flog = flog
        self.flog.set_module_name(__class__.__name__)

    @property
    def get_tests_location(self):
        return self.__tests_location

    def read_tests_dirs_names(self) -> Dict:
        self.flog.info("Looking for tests dirs in: " + self.__tests_location)
        abs_tests_dirs = {}
        dirs = os.listdir(self.__tests_location)
        for d in dirs:
            abs_tests_dirs[d] = os.path.join(self.__tests_location, d)
        return abs_tests_dirs

    def read_modules_from_dirs(self, dirs: Dict) -> Dict:
        files_dir_map = {}
        for k, v in dirs.items():
            module_files = []
            for f in os.listdir(v):
                if f.endswith("py") and not f.startswith("__"):
                    module_files.append(os.path.join(v, f))
            files_dir_map[k] = module_files
        return files_dir_map

    def find_pre_actions(self, files_dir_map: Dict)->PreActionSet:
        pre_actions = PreActionSet()
        for k in files_dir_map.keys():
            if k.startswith(PRE_ACTION_SUFFIX):
                if len(files_dir_map[k]) != 0:
                    dir_name = os.path.dirname(files_dir_map[k][0])
                    pre_action = Module(k, dir_name , files_dir_map[k], self.flog)
                    pre_actions.modules.append(pre_action)
                else:
                    self.flog.warning("Skipping module -->{}<-- does not have any tests".format(k))
        return pre_actions

    def find_post_actions(self, files_dir_map: Dict)->PostActionSet:
        post_actions = PostActionSet()
        for k in files_dir_map.keys():
            if k.startswith(POST_ACTION_SUFFIX):
                if len(files_dir_map[k]) != 0:
                    dir_name = os.path.dirname(files_dir_map[k][0])
                    post_action = Module(k, dir_name ,files_dir_map[k], self.flog)
                    post_actions.modules.append(post_action)
                else:
                    self.flog.warning("Skipping module -->{}<-- does not have any tests".format(k))
        return post_actions


    def find_tests(self, files_dir_map: Dict)->TestSet:
        tests = TestSet()
        for k in files_dir_map.keys():
            if k.startswith(TESTS_SUFFIX):
                if len(files_dir_map[k]) != 0:
                    dir_name = os.path.dirname(files_dir_map[k][0])
                    test = Module(k, dir_name, files_dir_map[k], self.flog)
                    tests.modules.append(test)
                else:
                     self.flog.warning("Skipping module -->{}<-- does not have any tests".format(k))
        return tests

    def read_modules(self) -> TestImporter:
        tests_dirs_names = self.read_tests_dirs_names()
        tests_dirs = self.read_modules_from_dirs(tests_dirs_names)
        tests = self.find_tests(tests_dirs)
        pre_actions = self.find_pre_actions(tests_dirs)
        post_actions = self.find_post_actions(tests_dirs)
        tests_with_actions = TestImporter(pre_actions, post_actions, tests, self.flog)
        return tests_with_actions


