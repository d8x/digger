from typing import List
from logger import FrameworkLogger
import inspect
import io
from contextlib import redirect_stdout


class MethodResult:

    def __init__(self, method_name: str):
        self.method_name = method_name
        self.printed_output = ""
        self.status = bool

    def __str__(self):
        output = "\nMethod Name: {}\nPrinted output: {}\nStatus: {}\n".format(
            self.method_name, self.printed_output, self.status)
        return output


class ClassResult:

    def __init__(self, class_name: str, number_of_tests: int):
        self.class_name = class_name
        self.number_of_tests = number_of_tests

        self.methods_results = list()
        self.status = bool
        self.number_of_passed_tests = int
        self.number_of_failed_tests = int

    def add_method_result(self, method_result: MethodResult):
        self.methods_results.append(method_result)


class Result:
    def __init__(self, module_name: str, module_directory: str, module_files: List[str]):
        self.module_name = module_name
        self.module_directory = module_directory
        self.module_files = module_files

        self.status = bool
        self.module_classes_result = list()
        self.module_setup_result = MethodResult
        self.module_teardown_result = MethodResult

    def add_class_result(self, class_result: ClassResult):
        self.module_classes_result.append(class_result)

    def add_setup_result(self, result: MethodResult):
        self.module_setup_result = result

    def add_teardown_result(self, result: MethodResult):
        self.module_teardown_result = result

    def __str__(self):
        output = "\nModule Name: {}\nModule Directory: {}\nModule Files: {}\nModule Status: {}\n".format(
            self.module_name, self.module_directory, self.module_files, self.status)
        return output


class Module:
    def __init__(self, name: str, directory: str, files: List[str], flog: FrameworkLogger):
        self.name = name
        self.directory = directory
        self.files = files
        self.executables = list()  # imported executables modules
        self.flog = flog
        self.flog.set_module_name(__class__.__name__)
        self.result = Result(name, directory, files)

    def __str__(self):
        return "Name: " + str(self.name) + " Directory: " + str(self.directory) + " Files: " + str(self.files)

    def run(self) -> Result:
        self.flog.info("Starting tests from module: {}".format(self.name))
        for m in self.executables:
            self.__setup_module_file(m)
            self.__run_module_file_tests(m)
            if not self.__teardown_module_file(m):
                self.flog.error("Teardown of module {} failed!".format(m.__name__))
                self.result.status = False
            self.result.status = True
        return self.result

    def __setup_module_file(self, imp_module) -> bool:
        self.flog.debug("Starting setup of: {}".format(imp_module.__name__))
        redirected_stdout = io.StringIO()
        method_result = MethodResult(imp_module.__name__)
        try:
            with redirect_stdout(redirected_stdout):
                imp_module.setup()
            method_output = redirected_stdout.getvalue()
            method_result.printed_output = method_output
            method_result.status = True
        except Exception as e:
            # FIXME Here should be defined different format of tests errors and exceptions
            self.flog.error("An error occured when running setup: {}".format(e))
            method_result.status = False
            return False
        self.result.add_setup_result(method_result)
        return True

    def __run_module_file_tests(self, imp_module):
        self.flog.debug("Starting tests of module: {}".format(imp_module.__name__))
        test_cls = inspect.getmembers(imp_module, inspect.isclass)
        for k in test_cls:
            cls_name = k[0]
            cls_instance = k[1]()
            module_methods = self._get_module_executable_methods(cls_instance)
            cls_result = ClassResult(cls_name, len(module_methods))
            self.flog.info("Running tests class: {}".format(cls_name))
            for name, method in module_methods.items():
                redirected_stdout = io.StringIO()
                method_result = MethodResult(name)
                try:
                    self.flog.debug("Executing method: {}".format(name))
                    with redirect_stdout(redirected_stdout):
                        method()
                    method_output = redirected_stdout.getvalue()
                    method_result.printed_output = method_output
                    method_result.status = True
                except Exception as e:
                    # FIXME Here should be defined different format of tests errors and exceptions
                    self.flog.error("An error occured when running test: {}".format(e))
                    method_result.status = False
                cls_result.add_method_result(method_result)
            self.result.add_class_result(cls_result)

    def _get_module_executable_methods(self, cls_instance):
        module_methods = inspect.getmembers(cls_instance, predicate=inspect.ismethod)
        executable_methods = {}
        for name, method in module_methods:
            self.flog.debug("Checking method name: {}".format(name))
            if not name.startswith("_"):
                executable_methods[name] = method
        return executable_methods

    def __teardown_module_file(self, imp_module) -> bool:
        self.flog.debug("Starting teardown of: {}".format(imp_module.__name__))
        redirected_stdout = io.StringIO()
        method_result = MethodResult(imp_module.__name__)
        try:
            with redirect_stdout(redirected_stdout):
                imp_module.setup()
            method_output = redirected_stdout.getvalue()
            method_result.printed_output = method_output
            method_result.status = True
        except Exception as e:
            # FIXME Here should be defined different format of tests errors and exceptions
            self.flog.error("An error occured when running teardown: {}".format(e))
            method_result.status = False
            return False
        return True

