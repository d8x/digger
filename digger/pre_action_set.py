
PRE_ACTION_SUFFIX = "pre_"


class PreActionSet:
    def __init__(self):
        self.modules = []

    def __str__(self):
        return self.modules
    
    def __len__(self):
        return len(self.modules)