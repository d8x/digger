from module import Result


class Formatter:
    def __init__(self, tests_result: Result):
        self.result = tests_result

    def create(self):
        return self.result


class RawFormatter(Formatter):
    pass


class ASCIFormatter(Formatter):
    pass


class HTMLFormatter(Formatter):
    pass
