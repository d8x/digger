from abc import ABCMeta, abstractmethod


class Report(metaclass=ABCMeta):

    def __init__(self, data, formatter):
        self.data = data
        self.formatter = formatter

    @abstractmethod
    def show(self):
        pass


class PreActionReport(Report):

    def show(self):
        print(self.data)


class TestReport(Report):
    def show(self):
        print(self.data)


class PostActionReport(Report):
    def show(self):
        print(self.data)


class FinalReport(Report):
    def show(self):
        print(self.data)
