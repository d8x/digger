from importer import TestImporter
from logger import FrameworkLogger
from pre_action_set import PreActionSet
from post_action_set import PostActionSet
from testset import TestSet
from module import Result
from typing import List


class TestExecutor:
    def __init__(self, flog: FrameworkLogger, formatter, tests_parallel: bool=False):
        self.tests_parallel = tests_parallel
        self.formatter = formatter
        self.flog = flog
        self.flog.set_module_name(__class__.__name__)

    def run(self, tests_builder: TestImporter):
        self.flog.info("Starting tests...")
        pre_action_results = self.__run_pre_actions(tests_builder.preModules)
        tests_results = self.__run_tests(tests_builder.tests)
        for k in tests_results:
            for c in k.module_classes_result:
                for m in c.methods_results:

                    print(m.printed_output)
        post_action_results = self.__run_post_actions(tests_builder.postModules)


    def __run_pre_actions(self, pre_actions: PreActionSet) -> List[Result]:
        self.flog.info("Found {} pre actions modules".format(len(pre_actions)))
        pre_actions_results = list()
        for m in pre_actions.modules:
            pre_actions_results.append(m.run())
        return pre_actions_results

    def __run_tests(self, main_tests: TestSet) -> List[Result]:
        self.flog.info("Found {} tests modules".format(len(main_tests)))
        tests_results = list()
        for k in main_tests.modules:
            tests_results.append(k.run())
        return tests_results

    def __run_post_actions(self, post_actions: PostActionSet) -> List[Result]:
        self.flog.info("Found {} post actions modules".format(len(post_actions)))
        post_actions_results = list()
        for m in post_actions.modules:
            post_actions_results.append(m.run())
        return post_actions_results
