import os
import tempfile

NEEDED_DISK_SPACE = 10240  # bytes

class CreateFile:
    parallel = True

    def create_file(self):
        print("Hello from SetUp ENv run")


def setup():
    print("SETUP of SetUp ENV")
    print("Checking needed space")
    disk = os.statvfs(tempfile.gettempdir())
    free_disk_space_in_bytes = disk.f_bavail * disk.f_frsize
    print("DISK SPACE: {} bytes".format(free_disk_space_in_bytes))
    if free_disk_space_in_bytes < NEEDED_DISK_SPACE:
        print("Not enough disk space")
        raise Exception("Not enough disk space")

def teardown():
    print("TearDown of SetUp ENV")